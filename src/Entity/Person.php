<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class Person
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Movie", mappedBy="persons")
     */
    private $movies;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profilePath;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $externId;

    public function __construct()
    {
        $this->movies = new ArrayCollection();
    }

    public function getId() : ? int
    {
        return $this->id;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getMovies() : Collection
    {
        return $this->movies;
    }

    public function addMovie(Movie $movie) : self
    {
        if (!$this->movies->contains($movie)) {
            $this->movies[] = $movie;
            $movie->addPerson($this);
        }

        return $this;
    }

    public function removeMovie(Movie $movie) : self
    {
        if ($this->movies->contains($movie)) {
            $this->movies->removeElement($movie);
            $movie->removePersonId($this);
        }

        return $this;
    }

    public function getName() : ? string
    {
        return $this->name;
    }

    public function setName(? string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    public function getProfilePath() : ? string
    {
        return $this->profilePath;
    }

    public function setProfilePath(? string $profilePath) : self
    {
        $this->profilePath = $profilePath;

        return $this;
    }

    public function getExternId() : ? int
    {
        return $this->externId;
    }

    public function setExternId(? int $externId) : self
    {
        $this->externId = $externId;

        return $this;
    }
}
