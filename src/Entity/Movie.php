<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $releaseDate;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", inversedBy="movies")
     */
    private $persons;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $externId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $synopsis;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $posterPath;

    public function __construct()
    {
        $this->persons = new ArrayCollection();
    }

    public function getId() : ? int
    {
        return $this->id;
    }

    public function getTitle() : ? string
    {
        return $this->title;
    }

    public function setTitle(? string $title) : self
    {
        $this->title = $title;

        return $this;
    }

    public function getReleaseDate() : ? string
    {
        return $this->releaseDate->format('Y-m-d');
    }

    public function setReleaseDate(? \DateTimeInterface $releaseDate) : self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getSynopsis() : ? string
    {
        return $this->synopsis;
    }

    public function setSynopsis(? string $synopsis) : self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPersons() : Collection
    {
        return $this->persons;
    }

    public function addPerson(Person $person) : self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
        }

        return $this;
    }

    public function removePerson(Person $person) : self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
        }

        return $this;
    }

    public function getExternId() : ? int
    {
        return $this->externId;
    }

    public function setExternId(? int $externId) : self
    {
        $this->externId = $externId;

        return $this;
    }

    public function getPosterPath() : ? string
    {
        return $this->posterPath;
    }

    public function setPosterPath(? string $posterPath) : self
    {
        $this->posterPath = $posterPath;

        return $this;
    }

}
