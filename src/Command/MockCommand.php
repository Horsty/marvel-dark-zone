<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use App\Entity\Movie;
use App\Entity\Person;
use Symfony\Component\Serializer\Encoder\ArrayEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class MockCommand extends ContainerAwareCommand
{
    // Uri utilisée pour le mock
    protected $uri = 'https://api.themoviedb.org/3/';

    // APIKey à déplacer dans les fichiers d'env
    private $apiKey = '96de0015745f3dabf2f5d8522a3595ce';

    protected function configure()
    {
        $this
        // the name of the command (the part after "bin/console")
            ->setName('app:get-mock')

        // the short description shown while running "php bin/console list"
            ->setDescription('Fill the database.')

        // the full command description shown when running the command with
        // the "--help" option
            ->setHelp('This command allows you to fill the database...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    { 
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln(['Fill DB']);
        $this->getMock();
        $output->writeln('Mock added');
    }

    public function getMock()
    {
        $this->getMovies();
        $this->getPeople();
        $this->randomAssoc();
    }

    public function getMovies()
    {
        $params = array(
            'language' => 'en-US',
            'sort_by' => 'popularity.desc',
            'include_adult' => false,
            'include_video' => false,
            'page' => 1,
            'year' => 2018
        );
        $queryString = \http_build_query($params);
        $response = $this->getCurl($this->uri . 'discover/movie?api_key=' . $this->apiKey . '&' . $queryString);
        $datas = json_decode($response);
        $entityManager = $this->getContainer()->get('doctrine')->getManager();
        foreach ($datas->results as $data) {
            $movie = new Movie();
            $movie->setTitle($data->title);
            $movie->setExternId($data->id);
            $movie->setPosterPath($data->poster_path);
            $movie->setSynopsis($data->overview);
            $movie->setReleaseDate(new \DateTime($data->release_date));
            $entityManager->persist($movie);
            $entityManager->flush();
        }
    }

    public function getPeople()
    {
        $response = $this->getCurl($this->uri . 'person/popular?api_key=' . $this->apiKey . '&language=en-US&page=1');
        $datas = json_decode($response);
        $entityManager = $this->getContainer()->get('doctrine')->getManager();
        foreach ($datas->results as $data) {
            if (!$data->adult) {
                $person = new Person();
                $person->setName($data->name);
                $person->setProfilePath($data->profile_path);
                $person->setExternId($data->id);
                $entityManager->persist($person);
                $entityManager->flush();
            }
        }
    }

    public function randomAssoc()
    {
        $entityManager = $this->getContainer()->get('doctrine')->getManager();
        $repository = $this->getContainer()->get('doctrine')->getRepository(Movie::class);
        $movies = $repository->findAll();
        $repository = $this->getContainer()->get('doctrine')->getRepository(Person::class);
        $people = $repository->findAll();
        // pour chaque acteurs on associe 2 film au hasard
        foreach ($people as $person) {
            $keys = array_rand($movies, 2);
            foreach ($keys as $key) {
                $person->addMovie($movies[$key]);
            }
        }
        $entityManager->flush();
    }

    public function getCurl(string $url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Cache-Control: no-cache',
                'Postman-Token: f063ecca-547c-4fc7-abef-fb048cf08b91'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo 'cURL Error #:' . $err;
        }

        return $response;
    }
}
