<?php

namespace App\Controller;

use App\Entity\Person;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


/**
 * @Route("/persons")
 */
class PersonController extends AbstractController
{
    /**
     * @Route("/{id<\d+>}", name="person_show", methods="GET")
     */
    public function show(Person $person) : Response
    {
        $serializer = $this->getSerialize();
        $data = $serializer->serialize($person, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * @Route("/", name="persons_list", methods="GET")
     */
    public function list()
    {
        $persons = $this->getDoctrine()->getRepository('App\Entity\Person')->findAll();
        $serializer = $this->getSerialize();
        $data = $serializer->serialize($persons, 'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

    /**
     * Instancie et retourne un serializer
     */
    public function getSerialize()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        // par défaut une exception est levée lors qu'on dépasse 1 niveau de déserialization
        $normalizer->setIgnoredAttributes(array('personId'));
        // possibilité d'ignorer le champ en question (à voir la gestion des accessors de Symfony)
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object;
        });
        return new Serializer(array($normalizer), array($encoder));
    }
}
