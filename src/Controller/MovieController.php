<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


/**
 * @Route("/movies")
 */
class MovieController extends AbstractController
{
    /**
     * @Route("/{id<\d+>}", name="movies_show", methods="GET")
     */
    public function show(Movie $movie) : Response
    {
        $serializer = $this->getSerialize();
        $data = $serializer->serialize($movie, 'json');
        $response = new Response($data);
        // * ou sous domaine 
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/", name="movies_list", methods="GET")
     */
    public function list()
    {
        $movies = $this->getDoctrine()->getRepository('App\Entity\Movie')->findAll();
        $serializer = $this->getSerialize();
        $data = $serializer->serialize($movies, 'json');

        $response = new Response($data);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Instancie et retourne un serializer
     */
    public function getSerialize()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        // par défaut une exception est levée lors qu'on dépasse 1 niveau de déserialization
        $normalizer->setIgnoredAttributes(array('movieId'));
        // possibilité d'ignorer le champ en question (à voir la gestion des accessors de Symfony)
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object;
        });
        return new Serializer(array($normalizer), array($encoder));
    }
}
