<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181009205207 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE moovie_person DROP FOREIGN KEY FK_1C5EE851E3B11CB5');
        $this->addSql('CREATE TABLE movie (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, release_date DATE DEFAULT NULL, extern_id INT DEFAULT NULL, synopsis LONGTEXT DEFAULT NULL, poster_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_person (movie_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_CD1B4C038F93B6FC (movie_id), INDEX IDX_CD1B4C03217BBB47 (person_id), PRIMARY KEY(movie_id, person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_person ADD CONSTRAINT FK_CD1B4C038F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_person ADD CONSTRAINT FK_CD1B4C03217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE moovie');
        $this->addSql('DROP TABLE moovie_person');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movie_person DROP FOREIGN KEY FK_CD1B4C038F93B6FC');
        $this->addSql('CREATE TABLE moovie (id INT AUTO_INCREMENT NOT NULL, extern_id INT DEFAULT NULL, poster_path VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, release_date DATE DEFAULT NULL, synopsis LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, title VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE moovie_person (moovie_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_1C5EE851217BBB47 (person_id), INDEX IDX_1C5EE851E3B11CB5 (moovie_id), PRIMARY KEY(moovie_id, person_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE moovie_person ADD CONSTRAINT FK_1C5EE851217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE moovie_person ADD CONSTRAINT FK_1C5EE851E3B11CB5 FOREIGN KEY (moovie_id) REFERENCES moovie (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE movie');
        $this->addSql('DROP TABLE movie_person');
    }
}
